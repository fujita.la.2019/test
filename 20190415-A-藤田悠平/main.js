//メッセージ一覧
const msgs = {
	1: 'おはよう',
	2: 'こんにちは',
	3: 'こんばんは'
};

/**
 * ボタンが押されたとき、引数に対応したメッセージを表示する
 * @param btnNum 押されたボタンの番号 
 */
function chMsg(btnNum) {
	document.getElementById('msg').value = msgs[btnNum];
}

//ここから追加仕様

/**
 * メッセージをランダムに表示する
 */
function chMsgByRandom() {
	//1~3の整数をランダムに取得
	let num = Math.floor(Math.random() * (4 - 1) + 1);
	chMsg(num);
}

/**
 * 現在時刻にあったメッセージを表示する
 */
function chMsgByTime() {
	//現在時刻（時）を取得
	let hour = new Date().getHours();
	let num;
	//時間帯による分岐
	if(5 <= hour && hour < 12) { //5~1１時は「おはよう」
		num = 1;
	}else if(hour < 18) { //12~17時は「こんにちは」
		num = 2;
	}else{ //18~4時は「こんばんは」
		num = 3;
	}
	chMsg(num);
}